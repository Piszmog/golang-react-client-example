import React, {Component} from 'react';
import axios from 'axios'
import './App.css';

function Movie(props) {
    return (
        <div>
            <div>Title: {props.title}</div>
            <div>Description: {props.description}</div>
        </div>
    );
}

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            description: null
        }
    }

    componentDidMount() {
        axios.get("http://localhost:8080/movies")
            .then(response => {

            });
    }

    async handleClick() {
        axios.get("http://localhost:8080/movies/d17fb1a3-fe76-4c19-8985-d8431f658e13")
            .then(response => {
                let data = response.data;
                this.setState({
                    name: data.name,
                    description: data.description
                });
            });
    }

    render() {
        return (
            <div className="App">
                <button onClick={() => this.handleClick()}>Call endpoint</button>
                <Movie title={this.state.name} description={this.state.description}/>
            </div>
        );
    }
}

export default App;
